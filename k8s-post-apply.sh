#!/bin/sh

set -e

echo "[burger-maker] wait for latest rollout of $environment_name to complete..."
kubectl rollout status deployment "$environment_name" --timeout=2m
